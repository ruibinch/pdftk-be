flask==1.0.3
flask-restful==0.3.7
flask-cors==3.0.8
google-cloud-storage==1.23.0
gunicorn==19.9.0
pytest==5.3.1
pyyaml==5.1.2
requests==2.22.0