FROM python:3.7-alpine

WORKDIR /app

# create a venv for application dependencies
RUN pip install virtualenv && \
    virtualenv -p python3.7 /venv
# set venv environment variables
# equivalent to running source /env/bin/activate
ENV VIRTUAL_ENV /venv
ENV PATH /venv/bin:$PATH

# install dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt && \
    mkdir logs

# add application code to /app
COPY . .

EXPOSE 5005

CMD ["gunicorn", "-w", "4", "-b", "0.0.0.0:5005", "wsgi:app"]