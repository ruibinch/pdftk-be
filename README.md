# PDF Toolkit - Backend

*Note: This project had been migrated from a private repository and hence does not include the pre-migration pipeline history.*

## Overview

The backend component is developed using **Python**. The [**Flask**](http://flask.palletsprojects.com/en/1.1.x/) web framework is used to expose a RESTful API interface to receive connections from the frontend. [**Gunicorn**](https://gunicorn.org/) is a WSGI HTTP server used to deploy the Flask application.

## Folder Structure

The project structure is based on the [Python backend repository skeleton template](https://gitlab.com/ecq-ai/skeleton-be).

The root folder contains the following sub-folders:

| Folder | Purpose |
| --- | --- |
| `config` | Configuration files |
| `core` | Main Python scripts |
| `ext` | External libraries |
| `logs` | Log files *(only used in development)* |
| `misc` | Misc files, e.g. PDF files used for testing |
| `notebooks` | Jupyter notebooks for initial Python code exploration |
| `tests` | Unit tests |
| `utils` | Utility files used across the codebase |

The following files are also located within the root folder:

| File | Purpose |
| --- | --- |
| `.dockerignore` | Files to be excluded in the docker image of the app |
| `.gitignore` | Files to be excluded in version control |
| `.gitlab-ci.yml` | Gitlab CI/CD pipeline configuration |
| `app.py` | Flask API definitions |
| `Dockerfile` | Dockerisation commands |
| `requirements.txt` | Python packages used in the codebase |
| `wsgi.py` | WSGI server setup |

## Logic Flow

The following flowchart depicts the overall logic flow in the backend.

![tkbe_logicflow](docs_tkbe_logicflow.png)

### Top-level

#### `app.py`

Contains the API endpoint definitions.

#### `main.py`

Contains the logic to route incoming requests to different modules/tools depending on the endpoint.

### Modules

#### `tenderiser.py`

Contains all the preprocessing functions in the **Tenderiser** module.

E.g. removing junk text, parsing into a suitable format for ingestion by the Elasticsearch server

#### `factsheet.py`

Contains all the functions relating to the **Factsheet Searcher** module.

E.g. regex searching

#### `changelog.py`

Retrieves the changelog list.

#### `feedback.py`

Contains all the functions relating to the **Feedback** module. Supports CRU operations on the feedback entries.

### Tools

#### `elasticsearch.py`

Contains all the methods relating to Elasticsearch API calls.

#### `gcs.py`

Manages all Google Cloud Storage operations, i.e. reading/writing from/to the GCS bucket.

#### `tika.py`

Contains all functions relating to the Tika server, i.e. extracting text from PDF files.

## External Libraries/References

### Elasticsearch

[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html) is used in the Tenderiser module to allow for fast searching of the text in PDF files.

Some Elasticsearch terminologies:

- **Index/Collection**: A collection of documents sharing somewhat similar characteristics; uniquely identified by a name
- **Document**: A basic unit of information that can be indexed
- **Analyser**: Converts text into *tokens* or *terms* that will be added to an Elasticsearch index for searching; made out of 3 lower-level building blocks ([reference link](https://www.elastic.co/guide/en/elasticsearch/reference/current/analyzer-anatomy.html))
    1. **Character filters**
        - Receives the original text as a stream of characters
        - Can transform the stream by adding, removing, or changing characters
        - An analyser may have **zero or more** character filters
    1. **Tokenisers**
        - Receives a stream of characters, breaks it up into individual tokens and outputs a stream of tokens
        - An analyser must have **exactly one** tokeniser 
    1. **Token filters**
        - Receives the token stream
        - Can add, remove or change tokens
        - An analyser may have **zero or more** token filters

2 main analysers are used in this app (refer to the `/core/tools/elasticsearch.py` file in the backend repository for reference):

1. Main analyser
    - Maps some characters to Unicode-appropriate characters
    - Tokenises the string stream by whitespace and the following characters
    - Applies the token filters - removes symbols and trailing fullstops, converts to lowercase
1. Symbol/punctuation analyser
    - Uses n-gram to identify and tokenise symbols and punctuation characters

#### Main analyser

##### Character filter

`char_mapping`: Maps some characters to Unicode-appropriate characters, to facilitate subsequent regex matching

```
"char_mapping": {
    "type": "mapping",
    "mappings": [
        "\u0091=>\u0027",
        "\u0092=>\u0027",
        "\u2018=>\u0027",
        "\u2019=>\u0027",
        "\uFF07=>\u0027",
        "“=>\"",
        "”=>\""
    ]
}
```

##### Tokeniser

`tokenizer_main`: Splits the text string into *tokens* on these set of characters - `-+=^&/\<>:;|` and *`whitespace`*

```
"tokenizer_main": {
    "type": "char_group",
    "tokenize_on_chars": [
        "whitespace",
        "-", "+", "=", "^", "&", "/", "\\", "<", ">", ":", ";", "|"
    ]
}
```

##### Token filter

`drop_symbol`: Removes symbols from the tokens - `[](){},;'"\|`

`drop_fullstop`: Removes trailing fullstops from the tokens

`lowercase`: Converts all characters to lowercase

```
"drop_symbol": {
    "type": "pattern_replace",
    "pattern": "[\\[\\](){},;'\"\\|]",
    "replacement": ""
},
"drop_fullstop": {
    "type": "pattern_replace",
    "pattern": "[.](?![0-9]+)",
    "replacement": ""
}
```

#### Symbol/punctuation analyser

##### Character filter

No character filter defined.

##### Tokeniser

`tokenizer_symbol`: Tokenises all symbols and punctuation to allow for searching by symbols like `$` and `%` 

```
"tokenizer_symbol": {
    "type": "ngram",
    "min_gram": 1,
    "max_gram": 1,
    "token_chars": [
        "symbol",
        "punctuation"
    ]
}
```

##### Token filter

No token filter defined.

#### Fields

Each clause, or *document*, is added to an Elasticsearch *index* under a field titled `entry`. This `entry` field has a `specialchars` sub-field.

Each field is processed using a different analyser:

| Field | Analyser |
| --- | --- |
| `entry` | `analyzer_main` |
| `entry.specialchars` | `analyzer_symbol` |

Depending on the search query, the search will be processed on a different field. This is so that the correct *tokens* will be searched on and the results returned will be accurate.

- If the search term is alphanumeric, search on the `entry` field
- If the search term is a special character (symbol, punctuation), search on the `entry.specialchars` field

```
"mappings": {
    "properties": {
        "entry": {
            "type": "text",
            "analyzer": "analyzer_main",
            "fields": {
                "specialchars": {
                    "type": "text",
                    "analyzer": "analyzer_symbol"
                }
            }
        }
    }
}
```


### Tika

The [**Apache Tika**](https://tika.apache.org/) tool is used to extract text from PDF files.

In the Kubernetes setup, a `tika` container containing an [Apache Tika docker image](https://hub.docker.com/r/logicalspark/docker-tikaserver/) is run in the same pod as the `tkbe` container.

### Google Cloud Storage

Changelog and feedback entries are stored in Google Cloud Storage in `changelog.json` and `feedback.json` files respectively.

A service account with "Storage Admin" privileges is used to authenticate read/write operations to the GCS bucket.

## Configuration

The `/config` folder stores all configuration files.

### External References

Any external references are stored in the `/config/endpoints.py` file.

The list of external references are:

- Elasticsearch host and port
    - References environment variables that are defined in a Kubernetes ConfigMap object
- Tika host and port
    - Tika is always accessible via `localhost` as the Tika server runs within the same pod in the deployment setup
- Filename of service account key with "Storage Admin" privileges
    - Used to access the Cloud Storage bucket
- Name of Cloud Storage bucket

### Logging

The logging configuration is stored in the `/config/logger.yml` file.

3 log levels are used - `DEBUG`, `INFO`, `ERROR`.

Reference:
- [Python Logging Guide](https://realpython.com/python-logging/)

## Testing Framework

Test cases will be written for the necessary modules in the `/core` folder. The test cases will be stored in the `/tests` folder and used by the in-built Python testing module `pytest`.

The structure of the `/tests` folder will mirror that in the `/core` folder.    The name of the test file will be the same as the Python file that it is performing tests on, with an additional `_test` suffix. This facilitates the matching of test cases to scripts, *e.g. the test file for `sample.py` should be titled `sample_test.py`*.

To run the test suite, run the following command in the root folder.

```
$ pytest
```