# ================================= FILE PATHS ================================

FILE_CHANGELOG = 'changelog.json'
FILE_FEEDBACK = 'feedback.json'

# =================================== STRINGS =================================

# dict key names for passing objects around
KEY_BODY = 'body'
KEY_CONTENT = 'content'
KEY_ERROR = 'error'
KEY_FEEDBACK_TYPE = 'feedbackType'
KEY_PARAMS = 'params'
KEY_PATH = 'path'
KEY_RESULTS = 'results'
KEY_STATUSCODE = 'statusCode'
KEY_SUBMITTER_NAME = 'submitterName'
# dict key names for "Factsheet Searcher" module
KEY_ITT_REF_NO = 'ITT Ref No'
KEY_DATE_CLOSED = 'Date Closed'
KEY_PROJECT_NAME = 'Project Name'
KEY_CUSTOMER = 'Customer'
KEY_DATE_PUBLISHED = 'Date Published'
KEY_CONTACT_PERSON = 'Contact Person'
KEY_CONTACT_DETAILS = 'Contact Details'

# error messages
MSG_ERROR_CONN_ES = 'Error connecting to Elasticsearch server'
MSG_ERROR_CONN_TIKA = 'Error connecting to Tika server'

STATUS_SUCCESS = 'success'
STATUS_FAIL = 'failed'

STR_ALL_OCCURRENCES = 'allOccurrences'
STR_INDIVWORDS = 'indivWords'
STR_PHRASE = 'phrase'
STR_PREFIX = 'prefix'

# ================================ ELASTICSEARCH ==============================

# Elasticsearch documents field names
FIELD_FILENAME = 'filename'
FIELD_CLAUSE = 'clause'
FIELD_SPECIALCHARS = 'specialchars'

SEARCHABLE_SYMBOLS = ['%', '$', '£', '€', '¥']