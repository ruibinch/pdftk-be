import os

"""
Stores all references to external endpoints.
"""

TIKA_IP = 'localhost'
TIKA_PORT = '9998'
