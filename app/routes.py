from flask import current_app, request
from flask_restful import Resource

from app.core import main

class Pdf2Txt(Resource):
    def post(self):
        files = request.files.to_dict() # dict of FileStorage objects

        output, status_code = main.convert_pdf_to_text(files)
        return output, status_code

class Health(Resource):
    def get(self):
        return 'OK', 200
