import os
import json
import logging

from .tools import tika
from app.utils import http_codes as http, strings

logger = logging.getLogger(__name__)

###############################################################################
#                                API FUNCTIONS                                #
###############################################################################

def convert_pdf_to_text(files):
    """POST /pdf2txt
    
    Args:
        files (dict): FileStorage objects
    """

    logger.debug('POST /pdf2txt')
    results_all = {}
    for key in files:
        # server-side validation that incoming file is a PDF
        if files[key].content_type != 'application/pdf':
            error_message = f'{key} is not a valid PDF-formatted file'
            logger.error(error_message)
            return {strings.KEY_ERROR: error_message}, http.HTTPCODE_INFO_INVALID

        # obtaining the file (in bytes) and filename
        file = files[key].read()
        filename = files[key].filename.split('/')[-1]

        text = tika.extract_text_from_pdf(file, filename)
        if isinstance(text, dict) and strings.KEY_ERROR in text:
            # error connecting to Tika server
            return text, http.HTTPCODE_RESOURCE_MISSING

        results_all[filename] = text
        
    return {strings.KEY_RESULTS: results_all}, http.HTTPCODE_OK
