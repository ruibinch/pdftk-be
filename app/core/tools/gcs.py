from google.cloud import storage
import json
import os

from app.config import endpoints
from app.utils import strings
import logging

logger = logging.getLogger(__name__)

"""
gcs.py manages all Google Cloud Storage operations.

Reference for explicit usage of service account credentials:
https://cloud.google.com/docs/authentication/production
"""

def get_file(root_path, filename):
    """Retrieves a file from the GCS bucket.
    
    Args:
        filename (str): Filename to read from
    """

    try:
        client = storage.Client.from_service_account_json(
            os.path.join(root_path, 'misc', endpoints.FILE_SERVICE_ACCOUNT)
        )

        bucket = client.get_bucket(endpoints.BUCKET_NAME)
        blob = bucket.blob(filename)
        string = blob.download_as_string()

        results = json.loads(string)
        logger.info(f'Reading file "{filename}" from GCS bucket "{endpoints.BUCKET_NAME}"')
        return {strings.KEY_RESULTS: results}
    except Exception as e:
        logger.error(f'Error reading file "{filename}" from GCS bucket "{endpoints.BUCKET_NAME}"', exc_info=True)
        raise Exception(e)

def write_file(root_path, filename, contents):
    """Writes a file to the GCS bucket.
    
    Args:
        filename (str): Filename to write to
    """

    try:
        client = storage.Client.from_service_account_json(
            os.path.join(root_path, 'misc', endpoints.FILE_SERVICE_ACCOUNT)
        )

        bucket = client.get_bucket(endpoints.BUCKET_NAME)
        blob = bucket.blob(filename)

        blob.upload_from_string(json.dumps(contents))
        logger.info(f'File "{filename}" uploaded to GCS bucket "{endpoints.BUCKET_NAME}"')
        return {strings.KEY_RESULTS: f'Write to GCS bucket "{endpoints.BUCKET_NAME}" successful'}
    except Exception as e:
        logger.error(f'Error reading file "{filename}" from GCS bucket "{endpoints.BUCKET_NAME}"', exc_info=True)
        raise Exception(e)