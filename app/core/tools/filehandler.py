import json
import logging

from app.utils import strings

logger = logging.getLogger(__name__)

"""
filehandler.py manages all file operations.

Only used in a testing environment.
"""

def read_json_file(filepath):
    """Reads in a JSON file stored on the system."""

    try:
        with open(filepath, 'r') as f:
            logger.info(f'Reading JSON file from {filepath}')
            results = json.load(f)
    except:
        error_message = f'Error reading JSON file from {filepath}'
        logger.error(error_message, exc_info=True)
        return {strings.KEY_ERROR: error_message}

    return {strings.KEY_RESULTS: results}

def write_to_json_file(filepath, content):
    """Writes to a JSON file stored on the system."""

    try:
        with open(filepath, 'w', encoding='utf-8') as f:
            logger.info(f'Updating JSON content at {filepath}')
            json.dump(content, f, ensure_ascii=False, indent=4)
    except:
        error_message = f'Error writing to JSON file at {filepath}'
        logger.error(error_message, exc_info=True)
        return {strings.KEY_ERROR: error_message}

    return {strings.KEY_RESULTS: 'Write to JSON file successful'}
