import json
import logging
import requests
from app.config import endpoints
from app.utils import strings

logger = logging.getLogger(__name__)

"""
tika.py contains all the functions relating to the Tika server.
"""

def parse_tika_response(text):
    """Parses the text response from the Tika server to extract the content."""
    text_json = json.loads(text)
    content = ''

    for js in text_json:
        if 'X-TIKA:content' in js:
            content += js['X-TIKA:content']
        
    return content

def extract_text_from_pdf(file, filename):
    """Uses the Tika JAR applet to extract text from the input PDF file.
    
    Args:
        file (bytes): PDF file in byte representation
        filename (str): Name of PDF file
    
    Returns:
        A text string of extracted text content.
        If there is an error, then return a dict with the "error" key.
    """

    tika_server_url = f'http://{endpoints.TIKA_IP}:{endpoints.TIKA_PORT}/rmeta/text'
    headers = {'Accept': 'application/json', 'Content-Disposition': f'attachment; filename={filename}'}
    content = ''
    
    logger.info(f'Connecting to Tika server: {tika_server_url}')
    logger.info(f'Extracting text from PDF: {filename}')

    try:
        response = requests.put(
            tika_server_url,
            file,
            headers=headers
        )
        content = parse_tika_response(response.text)
        return content
    except:
        logger.error(strings.MSG_ERROR_CONN_TIKA, exc_info=True)
        return {strings.KEY_ERROR: strings.MSG_ERROR_CONN_TIKA}