from app.utils import logger

"""
core.tools package contains the common tools used by various modules.
"""

logger.setup()