from flask import Flask
from flask_cors import CORS
from flask_restful import Api

def create_app():
    """Application factory.

    Returns the Flask application object.
    """

    app = Flask(__name__)
    CORS(app)

    with app.app_context():
        define_routes(app)
        return app

def define_routes(app: Flask):
    """Maps API endpoints to Flask resources.""" 

    from .routes import Health, Pdf2Txt

    api = Api(app)

    api.add_resource(Health, '/api/health')
    api.add_resource(Pdf2Txt, '/api/pdf2txt')
